package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var version = "v0.1.0"
var config string

var rootCmd = &cobra.Command{
	Use:     "gcu",
	Version: version,
	Short:   "Gcu provides easy handling of git global user.",
	Long: `Fast and easy way to handle your git global user by using simple yaml
file in your $HOME/.gcu.yaml as a storage.`,
}

func Execute() {
	rootCmd.AddCommand(add, edit, delete, show, use)
	rootCmd.PersistentFlags().StringVar(&config, "config", "", "specify path to the file used as a config.")
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
