package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/Azaradel/git-change-user/pkg/confighandler"
	"github.com/Azaradel/git-change-user/pkg/userchanger"
	"github.com/spf13/cobra"
)

var add = &cobra.Command{
	Use:   "add [user] [name] [email]",
	Short: "add user",
	Args:  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		c, err := confighandler.LoadConfig(config)
		if err != nil {
			log.Fatal(err)
		}
		err = c.AddUser(args[0], args[1], args[2])
		if err != nil {
			log.Fatal(err)
		}
		err = c.SaveConfig(c.Path)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("add successful", args[0], c.Users[args[0]])
		os.Exit(0)
	},
}

var delete = &cobra.Command{
	Use:   "delete [user]",
	Short: "delete user",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c, err := confighandler.LoadConfig(config)
		if err != nil {
			log.Fatal(err)
		}
		err = c.DeleteUser(args[0])
		if err != nil {
			log.Fatal(err)
		}
		err = c.SaveConfig(c.Path)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("delete successful", args[0])
		os.Exit(0)
	},
}

var edit = &cobra.Command{
	Use:   "edit [user] [name] [email]",
	Short: "edit user",
	Args:  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		c, err := confighandler.LoadConfig(config)
		if err != nil {
			log.Fatal(err)
		}
		err = c.EditUser(args[0], args[1], args[2])
		if err != nil {
			log.Fatal(err)
		}
		err = c.SaveConfig(c.Path)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("edit successful", args[0], c.Users[args[0]])
		os.Exit(0)
	},
}

var show = &cobra.Command{
	Use:   "show",
	Short: "show users in storage",
	Run: func(cmd *cobra.Command, args []string) {
		c, err := confighandler.LoadConfig(config)
		if err != nil {
			log.Fatal(err)
		}
		for k, v := range c.Users {
			fmt.Println(k, v)
		}
	},
}

var use = &cobra.Command{
	Use:   "use [user]",
	Short: "choose user preset to use as your git global user",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c, err := confighandler.LoadConfig(config)
		if err != nil {
			log.Fatal(err)
		}
		if _, ok := c.Users[args[0]]; !ok {
			log.Fatal("use: user not found")
		}
		name, email := c.Users[args[0]].Name, c.Users[args[0]].Email
		err = userchanger.BecomeUser(name, email)
		if err != nil {
			log.Fatal(err)
		}
		os.Exit(0)
	},
}
